import { InjectionToken } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { ApplicationContext } from '../models'

export const ApplicationContextToken = new InjectionToken<BehaviorSubject<ApplicationContext>>('Application Context')
