export class CheckboxList {
    code: string | number | boolean
    label: string
    selected: boolean
}
