import { AlertEnum } from '../enums'

export class Alert {
    message: string
    type: AlertEnum
    dismissible: boolean
    id: number
}
