import { User } from './user'

export class ApplicationContext {
    title: string
    titleCount?: number
    isLoggedIn?: boolean
    user?: User
    constructor() {
        this.title = ''
        this.isLoggedIn = false
    }
}
