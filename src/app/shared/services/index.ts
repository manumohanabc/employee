import { AlertService } from './alert.service'

export const SHARED_PROVIDERS = [AlertService]

export * from './alert.service'
