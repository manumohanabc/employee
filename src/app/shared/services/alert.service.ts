import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { AlertEnum } from '../enums'
import { Alert } from '../models'

@Injectable({
    providedIn: 'root',
})
export class AlertService {
    private _alerts: BehaviorSubject<Alert[]> = new BehaviorSubject<Alert[]>([])
    private _alerts$: Observable<Alert[]>
    constructor() {
        this._alerts$ = this._alerts.asObservable()
    }
    private setAlert(message: string, type: AlertEnum, dismissable: boolean) {
        const alert = new Alert()
        alert.message = message
        alert.type = type
        alert.dismissible = dismissable
        alert.id = Math.max(...this._alerts.getValue().map((item) => item.id)) + 1
        this._alerts.next([...this._alerts.getValue(), alert])
    }
    getAlerts(): Observable<Alert[]> {
        return this._alerts$
    }

    clearAllAlerts() {
        this._alerts.next([])
    }

    clearAllAlert(alert: Alert) {
        this._alerts.next([...this._alerts.getValue().filter((item) => item.id != alert.id)])
    }

    showErrorMessage(message: string, dismissable = true) {
        this.setAlert(message, AlertEnum.DANGER, dismissable)
    }
    showSuccessMessage(message: string, dismissable = true) {
        this.setAlert(message, AlertEnum.SUCCESS, dismissable)
    }
    showWarningMessage(message: string, dismissable = true) {
        this.setAlert(message, AlertEnum.WARNING, dismissable)
    }

    showInfoMessage(message: string, dismissable = true) {
        this.setAlert(message, AlertEnum.INFO, dismissable)
    }
}
