import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SHARED_COMPONENTS } from './components'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { ReactiveFormsModule } from '@angular/forms'
import { SHARED_PROVIDERS } from './services'

import { SAHRED_GUARDS } from './guards'
import { AlertModule } from 'ngx-bootstrap/alert'
import { ButtonsModule } from 'ngx-bootstrap/buttons'
import { ModalModule } from 'ngx-bootstrap/modal'
import { PopoverModule } from 'ngx-bootstrap/popover'
import { PaginationModule } from 'ngx-bootstrap/pagination'

@NgModule({
    declarations: [...SHARED_COMPONENTS],
    exports: [...SHARED_COMPONENTS],
    imports: [
        CommonModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        AlertModule.forRoot(),
        ButtonsModule.forRoot(),
        ModalModule.forRoot(),
        PopoverModule.forRoot(),
        PaginationModule.forRoot(),
    ],
    providers: [...SHARED_PROVIDERS, ...SAHRED_GUARDS],
})
export class SharedModule {}
