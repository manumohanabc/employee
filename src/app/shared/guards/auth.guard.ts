import { Inject, Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router'
import { BehaviorSubject, Observable } from 'rxjs'
import { ApplicationContext } from '../models'
import { ApplicationContextToken } from '../tokens'

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(readonly router: Router, @Inject(ApplicationContextToken) readonly appContext: BehaviorSubject<ApplicationContext>) {}
    canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
        return this.checkLogin()
    }

    canActivateChild(_childRoute: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
        return this.checkLogin()
    }

    private checkLogin(): boolean {
        if (!this.appContext.getValue().isLoggedIn) this.router.navigate(['login'])
        return this.appContext.getValue().isLoggedIn ? true : false
    }
}
