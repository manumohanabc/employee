import { Component, OnDestroy, OnInit } from '@angular/core'
import { takeUntil } from 'rxjs'
import { Alert } from '../../models'
import { AlertService } from '../../services'
import { BaseComponent } from '../base/base.component'

@Component({
    selector: 'emp-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss'],
})
export class AlertComponent extends BaseComponent implements OnInit, OnDestroy {
    alerts: Alert[] = []
    constructor(readonly alertService: AlertService) {
        super()
    }

    ngOnInit(): void {
        this.alertService
            .getAlerts()
            .pipe(takeUntil(this.$destroy))
            .subscribe((res) => {
                this.alerts = res
            })
    }

    onClosed(alert: Alert) {
        this.alertService.clearAllAlert(alert)
    }

    override ngOnDestroy(): void {
        super.ngOnDestroy()
    }
}
