import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core'
import { PageChangedEvent } from 'ngx-bootstrap/pagination'

@Component({
    selector: 'emp-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent {
    @Input() totalItems: number
    @Input() itemsPerPageList: number[] = [5, 10, 25, 50, 100]
    @Output() pageChange: EventEmitter<PageChangedEvent> = new EventEmitter()
    @Input() page: PageChangedEvent = {
        itemsPerPage: 5,
        page: 1,
    }
    constructor(readonly changeDetectRef: ChangeDetectorRef) {}

    pageChanged(event: PageChangedEvent) {
        this.page = event
        this.pageChange.emit(this.page)
    }
    onItemSelect(item: number) {
        this.page.itemsPerPage = item
        this.changeDetectRef.detectChanges()
        this.pageChange.emit(this.page)
    }
}
