import { Component, Input, OnDestroy } from '@angular/core'
import { getErrorMsg } from '../../utils'
import { InputBaseComponent } from '../base/input-base.component'

@Component({
    selector: 'emp-input-text-area',
    templateUrl: './input-text-area.component.html',
    styleUrls: ['./input-text-area.component.scss'],
})
export class InputTextAreaComponent extends InputBaseComponent implements OnDestroy {
    @Input() textRow = 5

    constructor() {
        super()
    }

    setErrorMsg(): void {
        this.error.next(getErrorMsg(this.control))
    }
    override ngOnDestroy(): void {
        super.ngOnDestroy()
    }
}
