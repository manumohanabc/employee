import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core'
import { PopoverDirective } from 'ngx-bootstrap/popover'
import { distinctUntilChanged, fromEvent, takeUntil } from 'rxjs'
import { BaseComponent } from '../base/base.component'

@Component({
    selector: 'emp-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
})
export class FilterComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('popover')
    readonly popover: PopoverDirective
    @Output() apply: EventEmitter<null> = new EventEmitter()
    @Output() reset: EventEmitter<null> = new EventEmitter()
    constructor() {
        super()
    }

    ngOnInit(): void {
        fromEvent(window, 'resize')
            .pipe(takeUntil(this.$destroy), distinctUntilChanged())
            .subscribe(() => {
                if (this.popover.isOpen) {
                    this.popover.hide()
                    this.popover.show()
                }
            })
    }
    /**
     * Method to handle apply filter
     */
    applyFilter() {
        this.apply.emit()
    }
    /**
     * method to hanlde reset filter
     */
    resetFilter() {
        this.reset.emit()
    }
    override ngOnDestroy(): void {
        super.ngOnDestroy()
    }
}
