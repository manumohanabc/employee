import { Component, Input, OnDestroy } from '@angular/core'
import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { getErrorMsg } from '../../utils'
import { InputBaseComponent } from '../base/input-base.component'

@Component({
    selector: 'emp-input-text',
    templateUrl: './input-text.component.html',
    styleUrls: ['./input-text.component.scss'],
})
export class InputTextComponent extends InputBaseComponent implements OnDestroy {
    @Input() icon: IconProp
    @Input() isPassword = false
    @Input() size = 'md'

    constructor() {
        super()
    }

    setErrorMsg() {
        this.error.next(getErrorMsg(this.control))
    }

    override ngOnDestroy(): void {
        super.ngOnDestroy()
    }
}
