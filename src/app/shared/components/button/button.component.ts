import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
    selector: 'emp-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
    @Input() size = 'md'
    @Input() disabled = false
    @Input() type = 'primary'
    @Input() isFullWidth = false
    @Output() submit: EventEmitter<null> = new EventEmitter()
    /**
     * Method to handle submit event
     */
    onSubmit() {
        this.submit.emit()
    }
}
