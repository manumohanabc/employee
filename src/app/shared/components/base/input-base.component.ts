import { Directive, Input, OnDestroy, OnInit } from '@angular/core'
import { AbstractControl, FormControl } from '@angular/forms'
import { BehaviorSubject, Observable, takeUntil } from 'rxjs'
import { Error } from '../../models'
import { BaseComponent } from './base.component'

@Directive()
export abstract class InputBaseComponent extends BaseComponent implements OnDestroy {
    @Input() hideLabel: boolean = false
    @Input() label: string
    @Input() id: string
    @Input() placeholder: string
    @Input() control: AbstractControl
    error: BehaviorSubject<Error | null> = new BehaviorSubject<Error | null>(null)
    error$: Observable<Error | null> = this.error.asObservable()
    currentError: Error | null
    constructor() {
        super()
        this.error$.pipe(takeUntil(this.$destroy)).subscribe((err) => {
            this.currentError = err
        })
    }
    /**
     * Method to get control
     */
    get _control() {
        return this.control as FormControl
    }
    /**
     * Method to handle onblur event
     */
    onBlur() {
        this.setErrorMsg()
    }

    abstract setErrorMsg(): void
    /**
     * Method to get validity of control
     */
    get isInValid(): boolean {
        return this.control.touched && this.control.invalid
    }
    /**
     * Method to cleanup component
     */
    override ngOnDestroy(): void {
        super.ngOnDestroy()
    }
}
