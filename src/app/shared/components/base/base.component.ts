import { Injectable, OnDestroy } from '@angular/core'
import { Subject } from 'rxjs'

@Injectable()
export abstract class BaseComponent implements OnDestroy {
    readonly $destroy: Subject<boolean> = new Subject<boolean>()
    constructor() {
        this.$destroy.next(true)
    }

    ngOnDestroy(): void {
        this.$destroy.next(true)
        this.$destroy.complete()
    }
}
