import { Component, Input } from '@angular/core'
import { AlertEnum } from '../../enums'

@Component({
    selector: 'emp-badge',
    templateUrl: './badge.component.html',
    styleUrls: ['./badge.component.scss'],
})
export class BadgeComponent {
    @Input() type: AlertEnum
    constructor() {}
}
