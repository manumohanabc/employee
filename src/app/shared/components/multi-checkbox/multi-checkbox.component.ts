import { Component, Input, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms'
import { List } from '../../models'

@Component({
    selector: 'emp-multi-checkbox',
    templateUrl: './multi-checkbox.component.html',
    styleUrls: ['./multi-checkbox.component.scss'],
})
export class MultiCheckboxComponent implements OnInit {
    @Input() list: List[]
    @Input() label: string
    @Input() control: FormGroup
    constructor(readonly fb: FormBuilder) {}

    ngOnInit(): void {
        this.control.addControl('items', new FormArray([]))
        this.list.forEach((item) => {
            ;(this.control.get('items') as FormArray).push(
                this.fb.group({
                    label: item.label,
                    selected: new FormControl(false),
                    code: item.code,
                }),
            )
        })
    }

    get formItems() {
        return (<FormArray>this.control.get('items')).controls
    }
}
