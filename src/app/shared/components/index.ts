import { ButtonComponent } from './button/button.component'
import { InputTextComponent } from './input-text/input-text.component'
import { InputTextAreaComponent } from './input-text-area/input-text-area.component'
import { CardComponent } from './card/card.component'
import { AlertComponent } from './alert/alert.component'
import { BadgeComponent } from './badge/badge.component'
import { FilterComponent } from './filter/filter.component'
import { MultiCheckboxComponent } from './multi-checkbox/multi-checkbox.component'
import { PaginationComponent } from './pagination/pagination.component'

export const SHARED_COMPONENTS = [
    ButtonComponent,
    InputTextAreaComponent,
    InputTextComponent,
    CardComponent,
    AlertComponent,
    BadgeComponent,
    FilterComponent,
    MultiCheckboxComponent,
    PaginationComponent,
]

export * from './button/button.component'
export * from './input-text/input-text.component'
export * from './input-text-area/input-text-area.component'
export * from './card/card.component'
export * from './base/input-base.component'
export * from './base/base.component'
export * from './alert/alert.component'
export * from './badge/badge.component'
export * from './filter/filter.component'
export * from './multi-checkbox/multi-checkbox.component'
export * from './pagination/pagination.component'
