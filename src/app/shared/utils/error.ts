import { AbstractControl } from '@angular/forms'
import { Error } from '../models'

export function getErrorMsg(control: AbstractControl): Error | null {
    let currentError: Error = new Error()
    const errors = control.errors
    if (errors) {
        if (errors['required']) {
            currentError.errorText = 'This field is required'
        }
        return currentError
    }
    return null
}
