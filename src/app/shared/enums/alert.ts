export enum AlertEnum {
    PRIMARY = 'primary',
    SUCCESS = 'success',
    DANGER = 'danger',
    WARNING = 'warning',
    INFO = 'info',
}
