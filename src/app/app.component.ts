import { Component } from '@angular/core'
import { Spinkit } from 'ng-http-loader'
@Component({
    selector: 'emp-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    spinner = Spinkit.skChasingDots
}
