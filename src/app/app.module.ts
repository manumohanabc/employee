import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { LoginComponent } from './login/login.component'
import { SharedModule } from './shared/shared.module'
import { AppBarComponent } from './app-bar/app-bar.component'
import { AppInterceptor, ApplicationContext, ApplicationContextToken } from './shared'
import { BehaviorSubject } from 'rxjs'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { NgHttpLoaderModule } from 'ng-http-loader'

@NgModule({
    declarations: [AppComponent, LoginComponent, AppBarComponent],
    imports: [
        AppRoutingModule,
        SharedModule,
        HttpClientModule,
        BrowserAnimationsModule,
        BrowserModule,
        BsDropdownModule.forRoot(),
        NgHttpLoaderModule.forRoot(),
    ],
    providers: [
        {
            provide: ApplicationContextToken,
            useValue: new BehaviorSubject<ApplicationContext>(new ApplicationContext()),
        },
        { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
