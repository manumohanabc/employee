import { Component, Inject, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { BehaviorSubject, takeUntil } from 'rxjs'
import { ApplicationContext, ApplicationContextToken, BaseComponent } from '../shared'

@Component({
    selector: 'emp-app-bar',
    templateUrl: './app-bar.component.html',
    styleUrls: ['./app-bar.component.scss'],
})
export class AppBarComponent extends BaseComponent implements OnInit {
    title: string | undefined
    titleCount: number | undefined
    isLoggedIn: boolean | undefined
    username: string | undefined
    constructor(@Inject(ApplicationContextToken) readonly appContext: BehaviorSubject<ApplicationContext>, readonly router: Router) {
        super()
    }

    ngOnInit(): void {
        this.appContext.pipe(takeUntil(this.$destroy)).subscribe((context: ApplicationContext) => {
            this.title = context.title
            this.titleCount = context.titleCount
            this.isLoggedIn = context.isLoggedIn
            this.username = context.user?.username
        })
    }

    onLogout() {
        this.appContext.next(new ApplicationContext())
        this.router.navigate(['login'])
    }
}
