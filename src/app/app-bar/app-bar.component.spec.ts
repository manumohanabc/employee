import { ComponentFixture, TestBed } from '@angular/core/testing'
import { Router } from '@angular/router'
import { BehaviorSubject } from 'rxjs'
import { ApplicationContext, ApplicationContextToken } from '../shared'

import { AppBarComponent } from './app-bar.component'

describe('AppBarComponent', () => {
    let component: AppBarComponent
    let fixture: ComponentFixture<AppBarComponent>
    const routerSpy = {
        navigate: jasmine.createSpy('navigate'),
    }
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppBarComponent],
            providers: [
                {
                    provide: ApplicationContextToken,
                    useValue: new BehaviorSubject<ApplicationContext>(new ApplicationContext()),
                },
                { provide: Router, useValue: routerSpy },
            ],
        }).compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(AppBarComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
