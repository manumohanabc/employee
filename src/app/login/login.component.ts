import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'
import { faLock } from '@fortawesome/free-solid-svg-icons'
import { BehaviorSubject, takeUntil } from 'rxjs'
import { ApplicationContext, ApplicationContextToken, BaseComponent } from '../shared'
import { AppConstants } from '../shared/constants'
import { AlertService } from '../shared/services'
@Component({
    selector: 'emp-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
    faEnvelope = faEnvelope
    faLock = faLock
    loginForm: FormGroup

    constructor(
        readonly fb: FormBuilder,
        readonly alertService: AlertService,
        readonly router: Router,
        @Inject(ApplicationContextToken) readonly appContext: BehaviorSubject<ApplicationContext>,
    ) {}
    ngOnInit(): void {
        this.appContext.next({ title: 'Login' })
        this.loginForm = this.fb.group({
            username: new FormControl(null, Validators.compose([Validators.required])),
            password: new FormControl(null, Validators.compose([Validators.required])),
        })
    }
    /**
     * Method to perform login functionality
     */
    onLogin() {
        this.alertService.clearAllAlerts()
        if (this.loginForm.valid) {
            if (
                AppConstants.loginConstants.username.toLocaleLowerCase() === this.loginForm.value.username.toLocaleLowerCase() &&
                AppConstants.loginConstants.password === this.loginForm.value.username
            ) {
                const context = this.appContext.getValue()
                context.isLoggedIn = true
                context.user = { username: this.loginForm.value.username }
                this.appContext.next(context)
                this.router.navigate(['dashboard'])
            } else this.alertService.showErrorMessage('incorrect username or password')
        } else this.alertService.showErrorMessage('Please enter username and password')
    }
    /**
     * Method to cleanup component
     *
     */
    ngOnDestroy(): void {
        this.alertService.clearAllAlerts()
    }
}
