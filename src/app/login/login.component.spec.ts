import { NO_ERRORS_SCHEMA } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ReactiveFormsModule } from '@angular/forms'
import { Router } from '@angular/router'
import { BehaviorSubject } from 'rxjs'
import { ApplicationContext, ApplicationContextToken } from '../shared'

import { LoginComponent } from './login.component'

describe('LoginComponent', () => {
    let component: LoginComponent
    let fixture: ComponentFixture<LoginComponent>
    const routerSpy = {
        navigate: jasmine.createSpy('navigate'),
    }
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                {
                    provide: ApplicationContextToken,
                    useValue: new BehaviorSubject<ApplicationContext>(new ApplicationContext()),
                },
                { provide: Router, useValue: routerSpy },
            ],
        }).compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
