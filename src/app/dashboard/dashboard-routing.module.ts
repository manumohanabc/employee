import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from '../shared'
import { EmployeeListComponent, EmployeeTasksComponent } from './components'

const routes: Routes = [
    {
        path: '',
        component: EmployeeListComponent,
    },
    {
        path: 'employeetasks/:id',
        component: EmployeeTasksComponent,
    },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule, SharedModule],
})
export class DashboardRoutingModule {}
