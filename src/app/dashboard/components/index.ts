import { EmployeeListComponent } from './employee-list/employee-list.component'
import { EmployeeTasksComponent } from './employee-tasks/employee-tasks.component'

export const DASHBOARD_COMPONETS = [EmployeeListComponent, EmployeeTasksComponent]

export * from './employee-list/employee-list.component'
export * from './employee-tasks/employee-tasks.component'
