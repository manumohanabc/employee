import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ActivatedRoute, Router } from '@angular/router'
import { BehaviorSubject } from 'rxjs'
import { ApplicationContext, ApplicationContextToken } from 'src/app/shared'
import { EmployeeServiceStub } from 'src/testing'
import { EmployeeService } from '../../services'

import { EmployeeListComponent } from './employee-list.component'

describe('EmployeeListComponent', () => {
    let component: EmployeeListComponent
    let fixture: ComponentFixture<EmployeeListComponent>
    const routerSpy = {
        navigate: jasmine.createSpy('navigate'),
    }
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EmployeeListComponent],
            providers: [
                {
                    provide: EmployeeService,
                    useClass: EmployeeServiceStub,
                },
                {
                    provide: ApplicationContextToken,
                    useValue: new BehaviorSubject<ApplicationContext>(new ApplicationContext()),
                },
                { provide: Router, useValue: routerSpy },
                {
                    provide: ActivatedRoute,
                    useValue: new ActivatedRoute(),
                },
            ],
        }).compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(EmployeeListComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
