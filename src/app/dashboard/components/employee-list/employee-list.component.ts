import { Component, Inject, OnDestroy, OnInit } from '@angular/core'
import { FormControl } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { PageChangedEvent } from 'ngx-bootstrap/pagination'
import { BehaviorSubject, noop, tap } from 'rxjs'
import { ApplicationContext, ApplicationContextToken } from 'src/app/shared'
import { Employee } from '../../models'
import { EmployeeService } from '../../services'

@Component({
    selector: 'emp-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit, OnDestroy {
    employees: Employee[] = []
    allEmployees: Employee[] = []
    searchControl = new FormControl()
    employeeCount: number
    page: PageChangedEvent = {
        itemsPerPage: 5,
        page: 1,
    }
    isLoaded: boolean
    isSearched: boolean
    constructor(
        readonly employeeService: EmployeeService,
        @Inject(ApplicationContextToken) readonly appContext: BehaviorSubject<ApplicationContext>,
        readonly router: Router,
        readonly route: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        const context = this.appContext.getValue()
        context.title = 'Employees'
        this.appContext.next(context)
        this.getEmployeeList()
        this.searchControl.valueChanges.subscribe((searchKey) => {
            this.employees = this.allEmployees.filter(
                (item) =>
                    item.email.toLowerCase().includes(searchKey) || item.name.toLowerCase().includes(searchKey) || item.phone.includes(searchKey),
            )
            this.isSearched = this.employees.length !== this.allEmployees.length
            this.setEmployeeCount()
            this.onPageChanged(this.page)
        })
    }
    /**
     * Method to get list of employees
     *
     */

    getEmployeeList() {
        this.isLoaded = false
        this.employeeService
            .getEmployeeList()
            .pipe(tap(noop, noop, () => (this.isLoaded = true)))
            .subscribe((res) => {
                this.employees = res
                this.allEmployees = res
                this.updateTitleCount()
                this.onPageChanged(this.page)
            })
    }
    /**
     * Method to update title count
     */
    updateTitleCount() {
        const context = this.appContext.getValue()
        context.titleCount = this.employees.length
        this.setEmployeeCount()
        this.appContext.next(context)
    }
    /**
     * Method to navigate to tasks page
     * @param id
     */
    navigateToTasks(id: number) {
        this.router.navigate(['employeetasks', id], { relativeTo: this.route })
    }
    /**
     * Method to handle pagination
     * @param page
     */
    onPageChanged(page: PageChangedEvent) {
        this.employees = this.isSearched
            ? this.employees.slice((page.page - 1) * page.itemsPerPage, page.page * page.itemsPerPage)
            : this.allEmployees.slice((page.page - 1) * page.itemsPerPage, page.page * page.itemsPerPage)
    }
    /**
     * Method to set employee count
     */

    setEmployeeCount() {
        this.employeeCount = this.employees.length
    }
    /**
     * Method to cleanup component
     */
    ngOnDestroy(): void {
        const context = this.appContext.getValue()
        context.titleCount = undefined
        this.appContext.next(context)
    }
}
