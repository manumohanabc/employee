import { Type } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ReactiveFormsModule } from '@angular/forms'
import { ActivatedRoute, ActivatedRouteSnapshot, Data, Params, UrlSegment } from '@angular/router'
import { BsModalService } from 'ngx-bootstrap/modal'
import { BehaviorSubject, Observable, of } from 'rxjs'
import { ApplicationContext, ApplicationContextToken } from 'src/app/shared'
import { BsModalServiceStub, EmployeeServiceStub } from 'src/testing'
import { EmployeeService } from '../../services'

import { EmployeeTasksComponent } from './employee-tasks.component'

describe('EmployeeTasksComponent', () => {
    let component: EmployeeTasksComponent
    let fixture: ComponentFixture<EmployeeTasksComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EmployeeTasksComponent],
            imports: [ReactiveFormsModule],
            providers: [
                {
                    provide: EmployeeService,
                    useClass: EmployeeServiceStub,
                },
                {
                    provide: ApplicationContextToken,
                    useValue: new BehaviorSubject<ApplicationContext>(new ApplicationContext()),
                },
                {
                    provide: ActivatedRoute,
                    useValue: { paramMap: of({}) },
                },
                {
                    provide: BsModalService,
                    useClass: BsModalServiceStub,
                },
            ],
        }).compileComponents()
    })

    beforeEach(() => {
        fixture = TestBed.createComponent(EmployeeTasksComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
