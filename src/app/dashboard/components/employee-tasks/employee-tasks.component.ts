import { Component, Inject, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { BehaviorSubject, noop, tap } from 'rxjs'
import { ApplicationContext, ApplicationContextToken, BaseComponent, CheckboxList } from 'src/app/shared'
import { AlertService } from 'src/app/shared/services'
import { DashboardConstants } from '../../constants/dashboard-constants'
import { Task } from '../../models'
import { EmployeeService } from '../../services'

@Component({
    selector: 'emp-employee-tasks',
    templateUrl: './employee-tasks.component.html',
    styleUrls: ['./employee-tasks.component.scss'],
})
export class EmployeeTasksComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild('template') templateRef: TemplateRef<any>
    allTasks: Task[] = []
    tasks: Task[] = []
    taskForm: FormGroup
    modalRef: BsModalRef
    searchControl = new FormControl()
    statusList = DashboardConstants.statusList
    statusForm: FormGroup = new FormGroup({})
    isFilterApplied = false
    searchKey: string
    isLoaded: boolean
    constructor(
        readonly employeeService: EmployeeService,
        @Inject(ApplicationContextToken) readonly appContext: BehaviorSubject<ApplicationContext>,
        readonly route: ActivatedRoute,
        readonly modalService: BsModalService,
        readonly fb: FormBuilder,
        readonly alertService: AlertService,
    ) {
        super()
    }

    ngOnInit(): void {
        const context = this.appContext.getValue()
        context.title = 'Employee Tasks'
        this.appContext.next(context)
        this.route.paramMap.subscribe((params) => {
            this.getEmployeeTasks(Number(params.get('id')))
        })
        this.taskForm = this.fb.group({
            task: new FormControl(null, Validators.compose([Validators.required])),
        })
        this.searchControl.valueChanges.subscribe((searchKey: string) => {
            this.searchKey = searchKey
            this.getTaskList()
        })
    }
    /**
     * Get list of employee tasks
     * @param employeeId
     */
    getEmployeeTasks(employeeId: number) {
        this.isLoaded = false
        this.employeeService
            .getEmployeeTasks(employeeId)
            .pipe(
                tap(noop, noop, () => {
                    this.isLoaded = true
                }),
            )
            .subscribe((res) => {
                this.allTasks = this.tasks = res
                this.updateTitleCount()
            })
    }

    /**
     * Get get task list
     */
    getTaskList() {
        if (this.searchKey || this.isFilterApplied) {
            if (this.searchKey) this.tasks = this.allTasks.filter((item) => item.title.toLowerCase().includes(this.searchKey.toLowerCase()))

            if (this.searchKey && this.isFilterApplied)
                this.tasks = this.tasks.filter((task) =>
                    (this.statusForm.value.items as Array<CheckboxList>).filter((item) => item.selected).some((item) => item.code === task.completed),
                )
            else if (this.isFilterApplied)
                this.tasks = this.allTasks.filter((task) =>
                    (this.statusForm.value.items as Array<CheckboxList>).filter((item) => item.selected).some((item) => item.code === task.completed),
                )
        } else this.tasks = this.allTasks
    }
    /**
     * Method to open modal
     */
    addNewtask() {
        this.alertService.clearAllAlerts()
        this.modalRef = this.modalService.show(this.templateRef, { ignoreBackdropClick: true, class: 'modal-lg modal-dialog-centered' })
    }
    /**
     * Method to close modal
     */
    closeModal() {
        this.taskForm.reset()
        this.modalRef.hide()
    }
    /**
     * Method to save new task
     */
    onSubmit() {
        if (this.taskForm.valid) {
            this.allTasks = [
                {
                    completed: false,
                    id: Math.max(...this.allTasks.map((item) => item.id)) + 1,
                    title: this.taskForm.value.task,
                },
            ].concat(this.allTasks)
            this.updateTitleCount()
            this.tasks = this.allTasks
            this.closeModal()
            this.alertService.showSuccessMessage('Task added successfully')
        }
    }
    /**
     * Method to update titile count
     */
    updateTitleCount() {
        const context = this.appContext.getValue()
        context.titleCount = this.allTasks.length
        this.appContext.next(context)
    }
    /**
     * Method to apply filter
     */
    applyFilter() {
        const statusValue: CheckboxList[] = (this.statusForm.get('items') as FormArray).value
        this.isFilterApplied = !statusValue.every((item) => item.selected == false)
        this.getTaskList()
    }
    /**
     * Method to reset filter
     */
    resetFilter() {
        for (let i = 0; i < (this.statusForm.get('items') as FormArray).length; i++) {
            ;(<FormArray>this.statusForm.get('items')).at(i).get('selected')?.setValue(false)
        }
        this.isFilterApplied = false
        this.getTaskList()
    }
    /**
     * Method to cleanup component
     */
    override ngOnDestroy(): void {
        const context = this.appContext.getValue()
        context.titleCount = undefined
        this.appContext.next(context)
        super.ngOnDestroy()
        this.alertService.clearAllAlerts()
    }
}
