import { EmployeeService } from './employee.service'

export const DASHBOARD_PROVIDERS = [EmployeeService]

export * from './employee.service'
