import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { Employee, Task } from '../models'

@Injectable({
    providedIn: 'root',
})
export class EmployeeService {
    private _baseUrl = '/api/users'
    constructor(readonly http: HttpClient) {}

    getEmployeeList(): Observable<Employee[]> {
        const url = `${this._baseUrl}`
        return this.http.get<Employee[]>(url)
    }
    getEmployeeTasks(employeeId: number): Observable<Task[]> {
        const url = `${this._baseUrl}/${employeeId}/todos`
        return this.http.get<Task[]>(url)
    }
}
