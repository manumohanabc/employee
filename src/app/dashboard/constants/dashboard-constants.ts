import { List } from 'src/app/shared'

export class DashboardConstants {
    public static get statusList(): List[] {
        return [
            {
                label: 'Completed',
                code: true,
            },
            {
                label: 'Not Completed',
                code: false,
            },
        ]
    }
}
