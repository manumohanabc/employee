import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { DashboardRoutingModule } from './dashboard-routing.module'
import { DASHBOARD_COMPONETS } from './components'
import { DASHBOARD_PROVIDERS } from './services'
import { SharedModule } from '../shared'

@NgModule({
    declarations: [...DASHBOARD_COMPONETS],
    imports: [CommonModule, DashboardRoutingModule, SharedModule],
    providers: [...DASHBOARD_PROVIDERS],
})
export class DashboardModule {}
