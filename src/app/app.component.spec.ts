import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BehaviorSubject } from 'rxjs'
import { AppComponent } from './app.component'
import { ApplicationContext, ApplicationContextToken } from './shared'

describe('AppComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [AppComponent],
            providers: [
                {
                    provide: ApplicationContextToken,
                    useValue: new BehaviorSubject<ApplicationContext>(new ApplicationContext()),
                },
            ],
        }).compileComponents()
    })

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent)
        const app = fixture.componentInstance
        expect(app).toBeTruthy()
    })
})
