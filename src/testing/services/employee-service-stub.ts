import { of } from 'rxjs'

export class EmployeeServiceStub {
    getEmployeeList() {
        return of([])
    }
}
